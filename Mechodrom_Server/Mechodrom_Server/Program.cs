﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Mechodrom_Server
{
    class Program
    {

        private TcpListener server;
        private TcpClient client;
        private List<Connection> list = new List<Connection>();

        private Random rnd = new Random();

        struct Connection
        {
            public NetworkStream stream;
            public StreamWriter streamw;
            public StreamReader streamr;
            public string nick;
            public string channel;
            public string AES_Key;
            public string RSA_PublicKey;
            public string RSA_PrivateKey;
        }

        static void Main(string[] args)
        {
            Program Prgm = new Program();
            Prgm.start();
        }

        void start()
        {
            Console.Title = "Mechodrom Server"; Console.WriteLine("Server is running!");
            server = new TcpListener(new IPEndPoint(IPAddress.Any, 1551)); server.Start();

            while (true)
            {
                client = server.AcceptTcpClient();
                Connection c = new Connection();
                c.stream = client.GetStream();
                c.streamr = new StreamReader(c.stream);
                c.streamw = new StreamWriter(c.stream);
                Cryption.RSA_key_Struct RSA_Key = Cryption.Create_RSA_Key();

                c.RSA_PublicKey = c.streamr.ReadLine();
                c.streamw.WriteLine(RSA_Key.open_key); c.streamw.Flush();
                c.RSA_PrivateKey = RSA_Key.privat_key;

                int p = int.Parse(Cryption.RSA_ent(CheckCerf(c.streamr.ReadLine(), c.streamr.ReadLine(), c), RSA_Key.privat_key));
                int g = int.Parse(Cryption.RSA_ent(CheckCerf(c.streamr.ReadLine(), c.streamr.ReadLine(), c), RSA_Key.privat_key));
                int b = rnd.Next(1, p - 2);
                int _B = Modular_Pow(g, b, p);
                c.AES_Key = Cryption.MD5StringHash(Modular_Pow(int.Parse(Cryption.RSA_ent(CheckCerf(c.streamr.ReadLine(), c.streamr.ReadLine(), c), RSA_Key.privat_key)), b, p).ToString());
                SendHandshakeString(Cryption.RSA_ver(_B.ToString(), c.RSA_PublicKey), c);
                c.nick = GetMessage(CheckCerf(c.streamr.ReadLine(), c.streamr.ReadLine(), c), c);
                c.channel = GetMessage(CheckCerf(c.streamr.ReadLine(), c.streamr.ReadLine(), c), c);
                Console.WriteLine("{0} has joined. Key: {1}. Channel: {2}. Prime: {3}. Primitiv Root: {4}", c.nick, c.AES_Key, c.channel, p, g);

                list.Add(c);
                SendNickToAllClients();

                Thread t = new Thread(delegate() { ListenToConnection(c); });
                t.Start();
            }
        }

        void ListenToConnection(Connection con)
        {
            while (true)
            {
                try
                {
                    string tmp = con.streamr.ReadLine();
                    string hash = con.streamr.ReadLine();
                    tmp = CheckCerf(tmp, hash, con);
                    tmp = Cryption.RijndaelDecrypt(tmp, con.AES_Key);

                    if (tmp == "!EXIT")
                    {
                        if (list.Contains(con))
                        {
                            list.Remove(con);
                            Console.WriteLine("{0} has exit.", con.nick);
                            SendNickToAllClients();
                            SendToAllClients(rnd.Next(100, 999) + con.nick + " has exit." + rnd.Next(100, 999));
                        }
                    }
                    else if (tmp.StartsWith("!MSG "))
                    {
                        Match m = Regex.Match(tmp.Replace("!MSG ", ""), "^([^\\s]+) (.*)");
                        foreach (Connection c in list)
                        {
                            if (c.nick == m.Groups[1].ToString())
                            {
                                Console.WriteLine("PM: from " + con.nick + "--> " + c.nick);
                                SendString(rnd.Next(100, 999) + "Private Message from: " + con.nick + " -> " + m.Groups[2].ToString() + rnd.Next(100, 999), c);
                            }
                        }

                    }
                    else if (tmp.StartsWith("!POKE "))
                    {
                        Match m = Regex.Match(tmp, "!POKE (.*) (.*) (.*)");
                        foreach (Connection c in list)
                        {
                            if (c.nick == m.Groups[1].ToString())
                            {
                                SendString("!POKE " + m.Groups[1].ToString() + " " + m.Groups[2].ToString() + " " + m.Groups[3].ToString(), c);
                            }
                        }

                    }
                    else if (tmp.StartsWith("!FILE"))
                    {
                        Match m = Regex.Match(tmp, "!FILE (.*) (.*) (.*)");
                        foreach (Connection c in list)
                        {
                            if (c.nick == m.Groups[1].ToString())
                            {
                                SendString("!FILE " + con.nick + " " + m.Groups[2].ToString() + " " + m.Groups[3].ToString(), c);
                                Console.WriteLine("!FILE " + con.nick + " " + m.Groups[2].ToString() + " " + m.Groups[3].ToString());
                            }
                        }
                    }
                    else if (tmp.StartsWith("!USERLIST ")) { }
                    else
                    {
                        if (tmp != "")
                        {
                            Console.WriteLine(con.nick + ": " + Cryption.Rijndaelcrypt(tmp, con.AES_Key));
                        }
                        SendToAllClientsExMe(tmp, con);
                    }
                }
                catch { }
            }
        }

        void SendToAllClientsExMe(string s, Connection con)
        {
            foreach (Connection c in list)
            {
                if (con.nick != c.nick && con.channel == c.channel)
                {
                    try
                    {
                        SendString(s, c);
                    }
                    catch { }
                }
            }
        }

        void SendToAllClients(string s)
        {
            foreach (Connection c in list)
            {
                try
                {
                    SendString(s, c);
                }
                catch { }
            }
        }

        void SendNickToAllClients()
        {
            foreach (Connection c in list)
            {
                string n = "";
                foreach (Connection con in list)
                {
                    if (c.channel == con.channel)
                    {
                        n += con.nick + ";";
                    }
                    try
                    {
                        SendString("!USERLIST " + n, c);
                    }
                    catch
                    {
                        list.Remove(c);
                        Console.WriteLine("{0} has exit.", c.nick);
                    }
                }
            }
        }

        int Modular_Pow(int a, int b, int m)
        {
            int result = 1;
            while (b > 0)
            {
                if ((b & 1) == 1)
                {
                    result = (result * a) % m;
                }
                b >>= 1;
                a = (a * a) % m;
            }
            return (result);
        }

        private string CheckCerf(string msg, string hash, Connection con)
        {
            string tmp_hash = Cryption.RSA_ent(hash, con.RSA_PrivateKey);
            string ret = "";
            if (Cryption.MD5StringHash(msg) != tmp_hash)
            {
                SendString(rnd.Next(100, 999) + "Server: Hijacked please leave now!" + rnd.Next(100, 999), con);
            }
            else
            {
                ret = msg;
            }
            return (ret);
        }

        void SendHandshakeString(string msg, Connection con)
        {
            con.streamw.WriteLine(msg);
            con.streamw.WriteLine(Cryption.RSA_ver(Cryption.MD5StringHash(msg), con.RSA_PublicKey));
            con.streamw.Flush();
        }

        void SendString(string msg, Connection con)
        {
            msg = Cryption.Rijndaelcrypt(msg, con.AES_Key);
            con.streamw.WriteLine(msg);
            con.streamw.WriteLine(Cryption.RSA_ver(Cryption.MD5StringHash(msg), con.RSA_PublicKey));
            con.streamw.Flush();
        }

        string GetMessage(string msg, Connection con)
        {
            return (Cryption.RijndaelDecrypt(msg, con.AES_Key));
        }
    }
}