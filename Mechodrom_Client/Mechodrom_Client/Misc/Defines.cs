﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.Devices;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using Mechodrom_Client.ChatClasses;
using System.Diagnostics;
using System.Collections;

namespace Mechodrom_Client.Misc
{
    public class Defines
    {

        #region Delegates
        public AppendText Delegate_AppendText;
        public ClearItems Delegate_ClearItems;
        public AddRange Delegate_AddRange;
        public Remove Delegate_Remove;
        public SelectionStart Delegate_SelectionStart;
        public SelectionColor Delegate_SelectionColor;
        public Dele_Mark Delegate_Mark;
        public Dele_AddItem Delegate_AddItem;
        public Dele_SaveFile Delegate_SaveFile;
        public ChangeLbl Delegate_ChangeLbl;

        public delegate void AppendText(string Text, RichTextBox t);
        public void InvokedProc_AppendText(string Text, RichTextBox t)
        {
            t.AppendText(Text);
            t.SelectionStart = t.TextLength;
            t.ScrollToCaret();
        }

        public delegate void ClearItems(ListBox t);
        public void InvokedProc_ClearItems(ListBox t)
        {
            t.Items.Clear();
        }

        public delegate void AddRange(string[] Text, ListBox t);
        public void InvokedProc_AddRange(string[] Text, ListBox t)
        {
            t.Items.AddRange(Text);
        }

        public delegate void Remove(string Text, ListBox t);
        public void InvokedProc_Remove(string Text, ListBox t)
        {
            t.Items.Remove(Text);
        }

        public delegate void SelectionStart(int Text, RichTextBox t);
        public void InvokedProc_SelectionStart(int Text, RichTextBox t)
        {
            t.SelectionStart = Text;
        }

        public delegate void SelectionColor(Color Text, RichTextBox t);
        public void InvokedProc_SelectionColor(Color Text, RichTextBox t)
        {
            t.SelectionColor = Text;
        }

        public delegate void Dele_Mark(RichTextBox rtb, string Text, Color t);
        public void InvokedProc_Mark(RichTextBox rtb, string Text, Color t)
        {
            rtb.SelectionStart = rtb.Text.Length;
            Color oldcolor = rtb.SelectionColor;
            rtb.SelectionColor = t;
            rtb.AppendText(Text);
            rtb.SelectionColor = oldcolor;
        }

        public delegate void Dele_AddItem(string Text, string Text2, ListView t);
        public void InvokedProc_AddItem(string Text, string Text2, ListView t)
        {
            ListViewItem lw = new ListViewItem();
            lw.Text = Text;
            lw.SubItems.Add(Text2);
            t.Items.Add(lw);
        }

        public delegate void Dele_SaveFile(string Text, string Name);
        public void InvokedProc_SaveFile(string Text, string Name)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = Name;
            sfd.ShowDialog();
            System.IO.File.WriteAllBytes(sfd.FileName, Convert.FromBase64String(Text));
        }

        public delegate void ChangeLbl(string Text, ToolStripLabel t);
        public void InvokedProc_ChangeLbl(string Text, ToolStripLabel t)
        {
            t.Text = Text;
        }
        #endregion

        public int sent = 0, received = 0;

        public Stopwatch sw = new Stopwatch();
        //public long[] sw_received = new long[256];
        public Hashtable sw_received = new Hashtable();
        public int sw_received_counter = 0;
        //public long[] sw_sent = new long[256];
        public Hashtable sw_sent = new Hashtable();
        public int sw_sent_counter = 0;

        public Random rnd = new Random();

        public Computer myComputer = new Computer();

        public Sendr sendr;
        public Certificater certificater;

        public string AES_Key;
        public NetworkStream stream;
        public StreamWriter streamw;
        public StreamReader streamr;
        public TcpClient client = new TcpClient();
        public CryptionClasses.Cryption.RSA_key_Struct RSA_key = CryptionClasses.Cryption.Create_RSA_Key();
        public int Diffi_p, Diffi_g, Diffi_a, Diffi__A;

        public Defines()
        {
            Delegate_AppendText = InvokedProc_AppendText;
            Delegate_ClearItems = InvokedProc_ClearItems;
            Delegate_AddRange = InvokedProc_AddRange;
            Delegate_Remove = InvokedProc_Remove;
            Delegate_SelectionStart = InvokedProc_SelectionStart;
            Delegate_SelectionColor = InvokedProc_SelectionColor;
            Delegate_Mark = InvokedProc_Mark;
            Delegate_AddItem = InvokedProc_AddItem;
            Delegate_SaveFile = InvokedProc_SaveFile;
            Delegate_ChangeLbl = InvokedProc_ChangeLbl;
        }
    }
}
