﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Mechodrom_Client.CryptionClasses
{
    public class Cryption
    {

        public struct RSA_key_Struct
        {
            public string open_key;
            public string privat_key;
            public string versus_open_key;
        }

        public static RSA_key_Struct Create_RSA_Key(int keysize = 2048)
        {
            RSA_key_Struct RSA_key = default(RSA_key_Struct);
            RSACryptoServiceProvider RSA_Crypto = new RSACryptoServiceProvider(keysize);
            RSA_key.open_key = RSA_Crypto.ToXmlString(false);
            RSA_key.privat_key = RSA_Crypto.ToXmlString(true);
            return RSA_key;
        }

        public static string RSA_ver(string DATA, string Openkey_Xml, int keysize = 2048)
        {
            RSACryptoServiceProvider RSA_Crypto_ver = new RSACryptoServiceProvider(keysize);
            byte[] byte_data = UTF8Encoding.UTF8.GetBytes(DATA);
            string Out_String = string.Empty;
            RSA_Crypto_ver.FromXmlString(Openkey_Xml);
            Out_String = Convert.ToBase64String(RSA_Crypto_ver.Encrypt(byte_data, true));
            return Out_String;
        }

        public static string RSA_ent(string DATA, string Privatekey_Xml, int keysize = 2048)
        {
            RSACryptoServiceProvider RSA_Crypto_ent = new RSACryptoServiceProvider(keysize);
            byte[] byte_data = Convert.FromBase64String(DATA);
            string Out_String = string.Empty;
            RSA_Crypto_ent.FromXmlString(Privatekey_Xml);
            Out_String = Encoding.Default.GetString(RSA_Crypto_ent.Decrypt(byte_data, true));
            return Out_String;
        }

        public static string Rijndaelcrypt(string File, string Key)
        {
            RijndaelManaged oAesProvider = new RijndaelManaged();
            byte[] btClear = null;
            byte[] btSalt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            Rfc2898DeriveBytes oKeyGenerator = new Rfc2898DeriveBytes(Key, btSalt);
            oAesProvider.Key = oKeyGenerator.GetBytes(oAesProvider.Key.Length);
            oAesProvider.IV = oKeyGenerator.GetBytes(oAesProvider.IV.Length);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, oAesProvider.CreateEncryptor(), CryptoStreamMode.Write);
            btClear = System.Text.Encoding.UTF8.GetBytes(File);
            cs.Write(btClear, 0, btClear.Length);
            cs.Close();
            File = Convert.ToBase64String(ms.ToArray());
            return (string)(File);
        }

        public static string RijndaelDecrypt(string UDecryptU, string UKeyU)
        {
            RijndaelManaged XoAesProviderX = new RijndaelManaged();
            byte[] XbtCipherX = null;
            byte[] XbtSaltX = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            Rfc2898DeriveBytes XoKeyGeneratorX = new Rfc2898DeriveBytes(UKeyU, XbtSaltX);
            XoAesProviderX.Key = XoKeyGeneratorX.GetBytes(XoAesProviderX.Key.Length);
            XoAesProviderX.IV = XoKeyGeneratorX.GetBytes(XoAesProviderX.IV.Length);
            System.IO.MemoryStream XmsX = new System.IO.MemoryStream();
            CryptoStream XcsX = new CryptoStream(XmsX, XoAesProviderX.CreateDecryptor(), CryptoStreamMode.Write);
            try
            {
                XbtCipherX = Convert.FromBase64String(UDecryptU);
                XcsX.Write(XbtCipherX, 0, XbtCipherX.Length);
                XcsX.Close();
                UDecryptU = System.Text.Encoding.UTF8.GetString(XmsX.ToArray());
            }
            catch
            {
            }
            return (string)(UDecryptU);
        }

        public static string MD5StringHash(string strString)
        {
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            byte[] Data = null;
            byte[] Result = null;
            string Res = "";
            string Tmp = "";

            Data = Encoding.ASCII.GetBytes(strString);
            Result = MD5.ComputeHash(Data);
            for (int i = 0; i <= Result.Length - 1; i++)
            {
                Tmp = Result[i].ToString("X");
                if (Tmp.Length == 1)
                    Tmp = "0" + Tmp;
                Res += Tmp;
            }
            return Res;
        }
    }
}
