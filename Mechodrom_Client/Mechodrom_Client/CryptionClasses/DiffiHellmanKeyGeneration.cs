﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Mechodrom_Client.CryptionClasses
{
    class DHG
    {

        public static int GenerateRandomPrimRoot(int prime)
        {
            int o = 1, k = 0, z = 0;
            Hashtable table = new Hashtable();
            int r = 2;
            while (r < prime)
            {
                k = Convert.ToInt32(Math.Pow(r, o));
                k = k % prime;
                while (k > 1)
                {
                    o = o + 1;
                    k = k * r;
                    k = k % prime;
                }
                if (o == (prime - 1))
                {
                    table[z] = r;
                    z = z + 1;
                }
                o = 1;
                r = r + 1;
            }
            return (int)(table[new Random().Next(1, z)]);
        }

        public static int GenerateRandomPrime(int max)
        {
            bool bol = false;
            Random rnd = new Random();
            int temp = 0;
            while (bol == false)
            {
                temp = rnd.Next(1, max);
                if (Fast(temp) == true)
                {
                    bol = true;
                }
            }
            return temp;
        }

        private static bool Fast(int n)
        {
            bool primJN = true;
            for (int i = 2; i <= (Math.Round(Math.Sqrt(n))); i++)
            {
                if (n % i == 0)
                {
                    primJN = false;
                    break;
                }
                else
                {
                    primJN = true;
                }
            }
            return (primJN);
        }

        private static string RandomString(Int64 Length)
        {
            System.Random rnd = new System.Random();
            StringBuilder Temp = new StringBuilder();
            for (Int64 i = 0; i < Length; i++)
            {
                Temp.Append(Convert.ToChar(((byte)rnd.Next(254))).ToString());
            }
            return Temp.ToString();
        }

        public static int Modular_pow(int a, int b, int m)
        {
            int result = 1;
            while (b > 0)
            {
                if ((b & 1) == 1)
                {
                    result = (result * a) % m;
                }
                b >>= 1;
                a = (a * a) % m;
            }
            return (result);
        }

    }
}
