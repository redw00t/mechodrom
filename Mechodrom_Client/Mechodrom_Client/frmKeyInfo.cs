﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mechodrom_Client.Misc;

namespace Mechodrom_Client
{
    public partial class frmKeyInfo : Form
    {
        public frmKeyInfo(string AES_Key, string privat_key, string open_key, string versus_open_key)
        {
            InitializeComponent();
            txt_aesKey.Text = AES_Key;
            txt_RSA_pri.Text = privat_key;
            txt_RSA_pub.Text = open_key;
            txt_RSA_opp.Text = versus_open_key;
        }
    }
}
