﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mechodrom_Client.Misc;

namespace Mechodrom_Client
{

    public partial class frmTraffic : Form
    {
        public frmTraffic(ref Defines D)
        {
            InitializeComponent();
            lbl_rec.Text = D.received.ToString();
            lbl_sent.Text = D.sent.ToString();
            for (int i = 0; i != D.sw_received_counter; i++)
            {
                chart1.Series["received"].Points.AddXY(i, D.sw_received[i]);
            }
            for (int i = 0; i != D.sw_sent_counter; i++)
            {
                chart1.Series["sent"].Points.AddXY(i, D.sw_sent[i]);
            }
        }
    }
}
