﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Mechodrom_Client.Misc;
using Mechodrom_Client.CryptionClasses;
using Mechodrom_Client.ChatClasses;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Sockets;

namespace Mechodrom_Client
{
    public partial class frmMain : Form
    {

        public Defines D = new Defines();

        public frmMain()
        {
            InitializeComponent();
            this.tabControl1.Region = new Region(new RectangleF(this.tabPage1.Left, this.tabPage1.Top, this.tabPage1.Width, this.tabPage1.Height));
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            if (txt_nickname.Text.Contains(" ") == false)
            {
                D.Diffi_p = DHG.GenerateRandomPrime(int.Parse(ud_prime.Value.ToString()));
                D.Diffi_g = DHG.GenerateRandomPrimRoot(D.Diffi_p);
                D.Diffi_a = D.rnd.Next(1, D.Diffi_p - 2);
                D.Diffi__A = DHG.Modular_pow(D.Diffi_g, D.Diffi_a, D.Diffi_p);

                try
                {
                    D.client.Connect(txt_serverip.Text, 1551);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("Could not connect to Server!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                D.stream = D.client.GetStream();
                D.streamr = new StreamReader(D.stream);
                D.streamw = new StreamWriter(D.stream);

                D.streamw.WriteLine(D.RSA_key.open_key); D.streamw.Flush();
                D.RSA_key.versus_open_key = D.streamr.ReadLine();

                Sendr.SendHandshakeString(Cryption.RSA_ver(D.Diffi_p.ToString(), D.RSA_key.versus_open_key), ref D.streamw, D.RSA_key);
                Sendr.SendHandshakeString(Cryption.RSA_ver(D.Diffi_g.ToString(), D.RSA_key.versus_open_key), ref D.streamw, D.RSA_key);
                Sendr.SendHandshakeString(Cryption.RSA_ver(D.Diffi__A.ToString(), D.RSA_key.versus_open_key), ref D.streamw, D.RSA_key);

                string tmp = D.streamr.ReadLine();
                string hash = D.streamr.ReadLine();
                if (Certificater.CheckCerf(tmp, hash, D.RSA_key) == true)
                {
                    D.AES_Key = Cryption.MD5StringHash(DHG.Modular_pow(int.Parse(Cryption.RSA_ent(tmp, D.RSA_key.privat_key)), D.Diffi_a, D.Diffi_p).ToString());
                }
                else
                {
                    this.Invoke(D.Delegate_AppendText, "Hijacked please leave now!", txt_rec);
                }

                D.sendr = new Sendr(D.RSA_key, D.AES_Key, ref D.streamw);
                D.certificater = new Certificater(D.RSA_key, D.AES_Key);

                D.sendr.SendString(txt_nickname.Text);
                D.sendr.SendString(txt_channel.Text);
                D.sendr.SendString(D.rnd.Next(100, 999) + txt_nickname.Text + " has joined." + D.rnd.Next(100, 999));

                Thread t = new Thread(new ThreadStart(Listen)); t.Start();

                lbl_stat_nickname.Text = txt_nickname.Text;
                lbl_stat_channel.Text = txt_channel.Text;

                tabControl1.SelectTab(1);
                lbl_top.Text = "Chat";
            }
            else
            {
                MessageBox.Show("Whitespaces are not allowed in Username!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void Listen()
        {
            D.sw.Start();
            while (D.client.Connected == true)
            {
                try
                {
                    string tmp = D.streamr.ReadLine();
                    string hash = D.streamr.ReadLine();
                    D.received++; D.sw_received[D.sw_received_counter] = (D.sw.ElapsedMilliseconds / 1000); D.sw_received_counter++;
                    if (Certificater.CheckCerf(tmp, hash, D.RSA_key) == false)
                    {
                        this.Invoke(D.Delegate_AppendText, "Hijacked please leave now!", txt_rec);
                    }

                    tmp = CryptionClasses.Cryption.RijndaelDecrypt(tmp, D.AES_Key);
                    if (tmp.StartsWith("!USERLIST "))
                    {
                        this.Invoke(D.Delegate_ClearItems, lb_online);
                        string[] arr = tmp.Replace("!USERLIST ", "").Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        this.Invoke(D.Delegate_AddRange, arr, lb_online);
                        this.Invoke(D.Delegate_Remove, txt_nickname.Text, lb_online);
                        this.Invoke(D.Delegate_ChangeLbl, arr.Count().ToString(), lbl_stat_user);
                    }
                    else if (tmp.StartsWith("!POKE "))
                    {
                        Match m = Regex.Match(tmp, "!POKE (.*) (.*) (.*)");
                        MessageBox.Show(m.Groups[2].ToString().Replace("_", " "), m.Groups[3].ToString() + " has poked you!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (tmp.StartsWith("!FILE "))
                    {
                        Match m = Regex.Match(tmp, "!FILE (.*) (.*) (.*)");
                        if (MessageBox.Show(m.Groups[1].ToString() + " has sent you a File(" + m.Groups[3].ToString() + "), would you like to save it?", "File received!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            this.Invoke(D.Delegate_SaveFile, m.Groups[2].ToString(), m.Groups[3].ToString());
                        }

                    }
                    else
                    {
                        tmp = tmp.Remove(0, 3); tmp = tmp.Substring(0, tmp.Length - 3);
                        if (this.TopMost == false)
                        {
                            Console.Beep();
                            if (tmp.StartsWith("Private Message from: "))
                            {
                                string Pat = "Private Message from: (.*) -> (.*)";
                                Match m = Regex.Match(tmp, Pat);
                                string str1 = m.Groups[1].ToString();
                                string str2 = m.Groups[2].ToString();
                                MainNotifyIcon.BalloonTipTitle = str1 + " sent you a Message!";
                                MainNotifyIcon.BalloonTipText = str2;
                            }
                            else
                            {
                                MainNotifyIcon.BalloonTipTitle = "New Message!";
                                MainNotifyIcon.BalloonTipText = tmp;
                            }
                            MainNotifyIcon.ShowBalloonTip(5000);
                        }
                        this.Invoke(D.Delegate_Mark, txt_rec, "[" + D.myComputer.Clock.LocalTime.Hour + ":" + D.myComputer.Clock.LocalTime.Minute + "] ", Color.DodgerBlue);
                        this.Invoke(D.Delegate_AppendText, tmp + "\n", txt_rec);
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        Console.WriteLine(ex.ToString());
                        this.Invoke(D.Delegate_AppendText, "Connection aborted!" + Environment.NewLine, txt_rec);
                    }
                    catch (Exception temp) { Console.WriteLine(temp); }
                }
            }
        }

        private void txt_in_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (txt_in.Text.Replace(" ", "") != "")
                {
                    D.sent++; D.sw_sent[D.sw_sent_counter] = (D.sw.ElapsedMilliseconds / 1000); D.sw_sent_counter++;
                    this.Invoke(D.Delegate_Mark, txt_rec, "[" + D.myComputer.Clock.LocalTime.Hour + ":" + D.myComputer.Clock.LocalTime.Minute + "] ", Color.Violet); txt_rec.AppendText("Me: "); this.Invoke(D.Delegate_AppendText, txt_in.Text + Environment.NewLine, txt_rec);
                    D.sendr.SendString(D.rnd.Next(100, 999) + txt_nickname.Text + ": " + txt_in.Text + D.rnd.Next(100, 999));
                    txt_in.Clear();
                }
                else
                {
                    MessageBox.Show("Please enter a Text!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_in.Clear();
                }
            }
        }

        private void sendPrivateMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string rec = lb_online.SelectedItem.ToString();
            string msg = Microsoft.VisualBasic.Interaction.InputBox("Please enter a Private Message..").ToString();
            D.sendr.SendString("!MSG " + rec + " " + msg);
            this.Invoke(D.Delegate_AppendText, "Private Message to : " + rec + " -> " + msg.Replace("_", " "), txt_rec);
        }

        private void pokeThisUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string rec = lb_online.SelectedItem.ToString();
            string msg = Microsoft.VisualBasic.Interaction.InputBox("Please enter a Poke Message..").ToString().Replace(" ", "_");
            D.sendr.SendString("!POKE " + rec + " " + msg + " " + txt_nickname.Text);
        }

        private void sendFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string rec = lb_online.SelectedItem.ToString();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            D.sendr.SendString("!FILE " + rec + " " + Convert.ToBase64String(System.IO.File.ReadAllBytes(ofd.FileName)) + " " + ofd.SafeFileName);
        }

        private void keyInformationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKeyInfo frm = new frmKeyInfo(D.AES_Key, D.RSA_key.privat_key, D.RSA_key.open_key, D.RSA_key.versus_open_key);
            frm.ShowDialog();
        }

        private void trafficToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTraffic frm = new frmTraffic(ref D);
            frm.ShowDialog();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                D.sendr.SendString("!EXIT");
            }
            catch (Exception temp) { Console.WriteLine(temp); }
            Environment.Exit(0);
        }

        #region Enter -> Tab
        private void txt_serverip_KeyDown(object sender, KeyEventArgs e)
        {
            CheckEnter(e);
        }

        private void txt_nickname_KeyDown(object sender, KeyEventArgs e)
        {
            CheckEnter(e);
            if ((txt_nickname.Text.Contains(" ") == false) || (txt_nickname.Text.EndsWith(" ") == true))
            {
                lbl_nickname.Text = "Nickname is allowed!";
                lbl_nickname.ForeColor = Color.Green;
            }
            else
            {
                lbl_nickname.Text = "Nickname is not allowed!";
                lbl_nickname.ForeColor = Color.Red;
            }
        }

        private void txt_channel_KeyDown(object sender, KeyEventArgs e)
        {
            CheckEnter(e);
        }

        void CheckEnter(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{tab}");
                e.Handled = true;
            }
        }
        #endregion

    }
}