﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mechodrom_Client.ChatClasses
{
    public class Certificater
    {

        private string K_ = "";
        private CryptionClasses.Cryption.RSA_key_Struct RSA_key_Struct_;

        public Certificater(CryptionClasses.Cryption.RSA_key_Struct RSA_key_Struct, string K)
        {
            RSA_key_Struct_ = RSA_key_Struct;
            K_ = K;
        }

        public static bool CheckCerf(string msg, string hash, CryptionClasses.Cryption.RSA_key_Struct rsa_key)
        {
            string tmp_hash = CryptionClasses.Cryption.RSA_ent(hash, rsa_key.privat_key);
            if (CryptionClasses.Cryption.MD5StringHash(msg) != tmp_hash)
            {
                return (false);
            }
            else
            {
                return (true);
            }
        }
    }
}
