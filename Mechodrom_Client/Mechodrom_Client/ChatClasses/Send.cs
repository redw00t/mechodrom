﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mechodrom_Client.ChatClasses
{
    public class Sendr
    {

        private CryptionClasses.Cryption.RSA_key_Struct RSA_key_Struct_;
        private string K_ = "";
        private StreamWriter streamw_;

        public Sendr(CryptionClasses.Cryption.RSA_key_Struct RSA_key_Struct, string K, ref StreamWriter streamw)
        {
            RSA_key_Struct_ = RSA_key_Struct;
            K_ = K;
            streamw_ = streamw;
        }

        public static void SendHandshakeString(string msg, ref StreamWriter streamw, CryptionClasses.Cryption.RSA_key_Struct RSA_key_Struct)
        {
            streamw.WriteLine(msg);
            streamw.WriteLine(CryptionClasses.Cryption.RSA_ver(CryptionClasses.Cryption.MD5StringHash(msg), RSA_key_Struct.versus_open_key));
            streamw.Flush();
        }

        public void SendString(string msg)
        {
            streamw_.WriteLine(CryptionClasses.Cryption.Rijndaelcrypt(msg, K_));
            streamw_.WriteLine(CryptionClasses.Cryption.RSA_ver(CryptionClasses.Cryption.MD5StringHash(CryptionClasses.Cryption.Rijndaelcrypt(msg, K_)), RSA_key_Struct_.versus_open_key));
            streamw_.Flush();
        }
    }
}
